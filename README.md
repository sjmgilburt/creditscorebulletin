# Credit Score Bulletin

In the United Kingdom, there are three main consumer credit reporting agencies: Callcredit, Equifax and Experian. 
Access to one's individual credit report and score is possible, free of charge, via 
[Noddle](https://www.noddle.co.uk/), [ClearScore](https://www.clearscore.com/) and Money Saving Expert's 
[Credit Club](https://www.moneysavingexpert.com/creditclub) respectively.  

This project provides automated access to all three credit reports, with the ultimate aim of providing a single, 
condensed overview email to the user. Current functionality is detailed in the release notes for the current version 
(see the [tags on this repository](https://gitlab.com/gilburtgtr/creditscorebulletin/tags) for versions).

This project has been developed by Samuel J. M. Gilburt under the MIT License.

## Getting Started

These instructions will set the project up and running on your local machine for development and testing purposes. See 
deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [Git](https://git-scm.com/)
* [Google Chrome](https://www.google.com/chrome)
* [Python 3.6](https://www.python.org/downloads) 
* [Virtualenv](https://virtualenv.pypa.io/)
* Accounts with [ClearScore](https://www.clearscore.com/), [MSE 
Credit Club](https://www.moneysavingexpert.com/creditclub) and [Noddle](https://www.noddle.co.uk/)


### Installation

1. Clone the repository
    ```
    git clone https://gitlab.com/gilburtgtr/creditscorebulletin.git
    ```

2. Create virtual environment
    ```
    cd creditscorebulletin
    virtualenv --python=python3.6 venv
    ```
    *Note: Windows users may have to specify full path to Python 3.6 interpreter executable, e.g.*  
    `--python=C:/path/to/python.exe`

3. Activate and install required packages  
    
    Linux/OS X:
    ```
    . venv/bin/activate
    pip install -r requirements.txt
    ```
    
    Windows:
    ```
    .\venv\Scripts\activate
    pip install -r requirements.txt
    ```
    
### Execution

Run `csb_main.py` (will prompt for credentials initialisation):
```
python csb_main.py
```


## Deployment

The current version is a development version only and should not be considered stable for wider deployment at this time. 

## Security

Please note that this program requires the user's confidential login information for each of the three services used to 
obtain their credit reports. This information is encrypted using a master password and stored on the user's 
machine. The master password is never stored and it is the responsibility of the user to keep their master password 
confidential. The stored login information will never be sent from the user's machine or be made accessible remotely to 
any individual. Nevertheless, this does not guarantee security and USE OF THIS PROGRAM IS AT THE USER'S OWN RISK (see 
the [LICENCE.md](LICENCE.md) file).

To delete all stored login information, destroy the `creds.json` file in the `creditscorebulletin` project folder. If 
a user believes their login information has been compromised, they should immediately change their passwords for each 
website by accessing their online accounts directly.

## Built with

* [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver) - WebDriver used for development
* [Cryptography](https://cryptography.io/) - Encryption of credentials
* [Selenium WebDriver](https://www.seleniumhq.org/projects/webdriver) - Automation of web browsing
* [XmlToolBox](https://xmltoolbox.appspot.com/) - XPath development

## Versioning

This project uses [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.com/gilburtgtr/creditscorebulletin/tags).

## Authors

See the list of [contributors](https://gitlab.com/gilburtgtr/creditscorebulletin/graphs/master) who participated in 
this project.

## Licence

This project is licensed under the MIT License - see the [LICENCE.md](LICENCE.md) file for details

## Acknowledgements

* [Frederica Caira](https://gitlab.com/freddie_caira) - inspiration, testing
* [Git Tower](https://www.git-tower.com/learn) - Git tutorials and documentation
* [Amy Morgan](https://thenounproject.com/amymorgan) - project icon
* [Andrea Saez](https://www.prodpad.com/blog/author/andrea-saez/) - release notes guide
* [Roger Taracha](https://medium.com/@thedancercodes) - Selenium introductory tutorial
* [Billie Thompson](https://github.com/PurpleBooth) - README.md file template
