#!/usr/bin/env python

"""Example of webscraping using Selenium, as produced from following Roger Taracha's tutorial on Medium:
https://medium.com/the-andela-way/introduction-to-web-scraping-using-selenium-7ec377a8cf72

The script opens an incognito Chrome window, scrapes a GitHub page for pinned repos' names and languages, and prints the
results.
"""

from selenium import webdriver                                    # Allows browser launch/initialisation
from selenium.webdriver.common.by import By                       # Allows search by parameters
from selenium.webdriver.support.ui import WebDriverWait           # Allows waiting for page to load
from selenium.webdriver.support import expected_conditions as ec  # Specify conditions to determine webpage load
from selenium.common.exceptions import TimeoutException           # Timeout handling

# Add incognito argument to webdriver
option = webdriver.ChromeOptions()
option.add_argument('-incognito')

# Create new instance of Chrome
browser = webdriver.Chrome(executable_path='../venv/Scripts/chromedriver', chrome_options=option)
browser.get("https://github.com/TheDancerCodes")

# Wait 20 seconds for page to load
timeout = 20

# Wait until avatar image is loaded (indicative of complete page load)
try:
    WebDriverWait(browser, timeout)\
        .until(ec.visibility_of_element_located((By.XPATH, "//img[@class='avatar width-full rounded-2']")))
except TimeoutException:
    print("Timed out waiting for page to load")
    browser.quit()

# find_elements_by_xpath returns an array of selenium objects
# Return the titles of all pinned repos by passing the relevant <a> tag and class combination
titles_element = browser.find_elements_by_xpath("//a[@class='text-bold']")

# Use list comprehension to get the actual repo titles and not the selenium objects
titles = [x.text for x in titles_element]

# Print all titles
print('titles:')
print(titles, '\n')

# Repeat above for languages of pinned repos (this time using <p> tag)
language_element = browser.find_elements_by_xpath("//p[@class='mb-0 f6 text-gray']")
languages = [x.text for x in language_element]
print("languages:")
print(languages, '\n')

# Zip and print all
for title, language in zip(titles, languages):
    print("RepoName : Language")
    print(title + ": " + language, '\n')

# Note: This printing of results here is rather inelegant (e.g. does not loop through the comprehensions)
