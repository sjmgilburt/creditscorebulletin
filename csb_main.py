#!/usr/bin/env python3.6

"""Main module for the Credit Score Bulletin project, which provides credit scores from the three UK credit agencies
(see README.md).

This program is designed to scrape the three UK credit agencies and return them to the user via the terminal. Firstly it
generates an encrypted creds.json file which stores user credentials for each provider (if one does not already exist).
Then runs the scrape_score methods of each of the scraper modules in the csb package, returning the scores and printing
each to the terminal.

Ultimately, the aim is for this program to generate an email bulletin to be sent to the user, with the intention of this
being possible to automate monthly.
"""

from getpass import getpass
import json
import os
import sys

import csb.clearscore_scraper as cls
import csb.creditclub_scraper as crc
import csb.creds as crd
import csb.noddle_scraper as nod


# Set max scores for each agency
callcredit_max_score = 710
equifax_max_score = 700
experian_max_score = 999

# Check if creds.json initialised and if not, create
if not os.path.exists('creds.json'):
    crd.gen_creds()
    print()

# Load encrypted credentials from creds.json file
with open('creds.json', 'r') as creds_file:
    creds = json.load(creds_file)

    with open('NaCl', 'rb') as salt_file:
        salt = salt_file.read()

    # Prompt user for master password and create Fernet for creds decryption
    if sys.stdin.isatty():
        f, _ = crd.create_fernet(getpass("Master password: ").encode(), salt)
    else:
        print(
            'Master password (BEWARE key will be visible in console. Please use a TTY terminal to avoid this.):')
        f, _ = crd.create_fernet(sys.stdin.readline().rstrip().encode(), salt)

    # Scrape scores
    print("Obtaining Callcredit score...")
    callcredit_score = nod.scrape_score(
        f.decrypt(creds['Noddle']['usr'].encode()).decode(),
        f.decrypt(creds['Noddle']['pwd'].encode()).decode())

    print("Obtaining Equifax score...")
    equifax_score = cls.scrape_score(
        f.decrypt(creds['ClearScore']['usr'].encode()).decode(),
        f.decrypt(creds['ClearScore']['pwd'].encode()).decode())

    print("Obtaining Experian score...")
    experian_score = crc.scrape_score(
        f.decrypt(creds['MSE Credit Club']['usr'].encode()).decode(),
        f.decrypt(creds['MSE Credit Club']['pwd'].encode()).decode(),
        f.decrypt(creds['MSE Credit Club']['mwd'].encode()).decode())

    # Ensure Fernet closed
    f = None

# Print scores
print("Callcredit credit score: " + str(callcredit_score) + " / " + str(callcredit_max_score))
print("Equifax credit score:    " + str(equifax_score) + " / " + str(equifax_max_score))
print("Experian credit score:   " + str(experian_score) + " / " + str(experian_max_score))
