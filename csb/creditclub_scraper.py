#!/usr/bin/env python3.6

"""Scraper to return credit score from MSE Credit Club (https://www.moneysavingexpert.com/creditclub)

This module contains one function - scrape_score - which takes the user's username, password and memorable word and
returns their Experian credit score via Money Saving Expert's Credit Club. This module is intended to be used as called
from csb_main.py, however it is also executable and will return the Experian credit score in a readable format to the
terminal/console window.
"""


from getpass import getpass
import json
import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from csb.creds import create_fernet


def scrape_score(usr, pwd, mwd):
    """Takes as parameters the username, password and memorable word to be used to login to MSE Credit Club
    (https://www.moneysavingexpert.com/creditclub) and returns Experian credit score via using the Selenium webdriver
    framework. In the event of a browser timeout, the function returns -1.
    """

    timeout = 60

    # Set up ChromeDriver and go to MSE Credit Club login page
    options = webdriver.ChromeOptions()
    options.add_argument('-incognito')
    options.add_argument("--log-level=3")
    browser = webdriver.Chrome(executable_path='./webdrivers/chromedriver', chrome_options=options)
    browser.maximize_window()
    browser.get("https://clubs.moneysavingexpert.com/creditclub/login")

    # Wait until Email and Password fields loaded
    try:
        WebDriverWait(browser, timeout).until(ec.visibility_of_all_elements_located(
            (By.XPATH, ("//input[@type='email' or " +
                        "@class='input--block ng-pristine ng-untouched showpassword--input-padding ng-empty " +
                        "ng-invalid ng-invalid-required ng-valid-maxlength']"))))
    except TimeoutException:
        print("ERROR: Timed out waiting for MSE Credit Club site response. " + 
              "Email and/or password field could not be found.")
        try:
            browser.quit()
        except ConnectionAbortedError:
            return -1

    # Log in
    email = browser.find_element_by_xpath("//input[@type='email']")
    password = browser.find_element_by_xpath("//input[@class='input--block ng-pristine ng-untouched " +
                                             "showpassword--input-padding ng-empty ng-invalid " +
                                             "ng-invalid-required ng-valid-maxlength']")
    email.send_keys(usr)
    password.send_keys(pwd)
    browser.find_element_by_id('sign-in').click()

    # Wait for memorable word char boxes to load, then input as decrypted from creds.json and log in
    try:
        WebDriverWait(browser, timeout).until(ec.visibility_of_all_elements_located(
            (By.XPATH, (
                "//input[@id='memorableWordChar1' or @id='memorableWordChar2' or @id='memorableWordChar3']"))))

        # Obtain chars indices required
        lab = browser.find_element_by_xpath("//label[@class='input-group__label input-group__label--stacked']").text
        inds = [int(lab[17])-1, int(lab[22])-1, int(lab[28])-1]
        mwd_boxes = [
            browser.find_element_by_id('memorableWordChar1'),
            browser.find_element_by_id('memorableWordChar2'),
            browser.find_element_by_id('memorableWordChar3')
        ]
        for i in range(0, 3):
            mwd_boxes[i].send_keys(mwd[inds[i]])

        browser.find_element_by_id('sign-in-2FA-authentication').click()

    except TimeoutException:
        print("ERROR: Timed out waiting for MSE Credit Club site response. " +
              "Memorable word char fields could not be found.")
        try:
            browser.quit()
        except ConnectionAbortedError:
            return -1

    # Wait until Experian Score loaded, then return it
    try:
        WebDriverWait(browser, timeout).until(ec.visibility_of_element_located(
            (By.XPATH, "//span[@class='experian-score__score-text--large ng-binding']")))
        score = int(browser.find_element_by_xpath("//span[@class='experian-score__score-text--large ng-binding']").text)
    except TimeoutException:
        print("ERROR: Timed out waiting for MSE Credit Club site response. " +
              "Credit score text element could not be found.")
        try:
            browser.quit()
        except ConnectionAbortedError:
            return -1

    # Quit the browser and return score
    browser.quit()
    return score


# Prevent execution from import statement
if __name__ == "__main__":
    # Load encrypted credentials from creds.json file
    with open('creds.json', 'r') as creds_file:
        creds = json.load(creds_file)

        with open('NaCl', 'rb') as salt_file:
            salt = salt_file.read()

        # Prompt user for master password and create Fernet for creds decryption
        if sys.stdin.isatty():
            f, _ = create_fernet(getpass("Master password: ").encode(), salt)
        else:
            print(
                'Master password (BEWARE key will be visible in console. Please use a TTY terminal to avoid this.):')
            f, _ = create_fernet(sys.stdin.readline().rstrip().encode(), salt)

        # Scrape score and print
        experian_max_score = 999
        score = scrape_score(
            f.decrypt(creds['MSE Credit Club']['usr'].encode()).decode(),
            f.decrypt(creds['MSE Credit Club']['pwd'].encode()).decode(),
            f.decrypt(creds['MSE Credit Club']['mwd'].encode()).decode())
        print("Experian credit score: " + str(score) + " / " + str(experian_max_score))
