"""Module that provides functions for generation, encryption and decryption of user credentials (stored in creds.json)
as part of the Credit Score Bulletin project.
"""

import base64
from getpass import getpass
import json
import sys
import os

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def create_fernet(pwd, salt=os.urandom(16)):
    """Returns a Fernet object for cryptography created from input bytes and optional salt specification (for
    decryption)
    """

    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend())
    key = base64.urlsafe_b64encode(kdf.derive(pwd))
    return Fernet(key), salt


def gen_creds():
    """Prompts user to input credentials and generates encrypted creds.json file"""

    # Create empty dictionary for credentials for each provider
    creds = {
        "ClearScore": dict(usr="", pwd=""),
        "MSE Credit Club": dict(usr="", pwd="", mwd=""),
        "Noddle": dict(usr="", pwd="")
    }

    print("No credentials found. Please initialise (your details will be encrypted).")

    # Prompt user to set master password (note: getpass() does not function correctly in non-TTY consoles)
    if sys.stdin.isatty():
        f, salt = create_fernet(getpass("Set master password: ").encode())
    else:
        print('Set encryption key (BEWARE key will be visible in console. Please use a TTY terminal to avoid this.):')
        f, salt = create_fernet(sys.stdin.readline().rstrip().encode())

    # Obtain login details from user and encrypt
    for site in creds:
        print(site + ":")
        creds[site]['usr'] = f.encrypt(input("    Email/user: ").encode()).decode()
        if sys.stdin.isatty():
            creds[site]['pwd'] = f.encrypt(getpass("    Password: ").encode()).decode()
        else:
            print(
                '    Password (BEWARE password will be visible in console. Please use a TTY terminal to avoid this.):')
            creds[site]['pwd'] = f.encrypt(sys.stdin.readline().rstrip().encode()).decode()

        if site == "MSE Credit Club":
            if sys.stdin.isatty():
                creds[site]['mwd'] = f.encrypt(getpass("    Memorable word: ").encode()).decode()
            else:
                print(
                    '    Memorable word (BEWARE password will be visible in console. Please use a TTY terminal to ' +
                    'avoid this.):')
                creds[site]['mwd'] = f.encrypt(sys.stdin.readline().rstrip().encode()).decode()

    # Ensure fernet is closed
    f = None

    # Persist to JSON file
    with open('creds.json', 'w') as outfile:
        json.dump(creds, outfile)

    # Persist salt
    with open('NaCl', 'wb') as outfile:
        outfile.write(salt)
