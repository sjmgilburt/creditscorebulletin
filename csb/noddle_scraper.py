#!/usr/bin/env python3.6

"""Scraper to return credit score from Noddle (https://www.noddle.co.uk/)

This module contains one function - scrape_score - which takes the user's username and password and returns
their Callcredit credit score via Noddle. This module is intended to be used as called from csb_main.py, however it is
also executable and will return the Callcredit credit score in a readable format to the terminal/console window.
"""


from getpass import getpass
import json
import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from csb.creds import create_fernet


def scrape_score(usr, pwd):
    """Takes as parameters the username and password to be used to login to Noddle (https://www.noddle.co.uk/) and
    returns Callcredit credit score using the Selenium webdriver framework. In the event of a browser timeout, the
    function returns -1.
    """

    timeout = 60
    popup_timeout = 2

    # Set up ChromeDriver and go to Noddle login page
    options = webdriver.ChromeOptions()
    options.add_argument('-incognito')
    options.add_argument("--log-level=3")
    browser = webdriver.Chrome(executable_path='./webdrivers/chromedriver', chrome_options=options)
    browser.maximize_window()
    browser.get("https://www.noddle.co.uk/account/sign-in")

    # Wait until Username and Password fields loaded
    try:
        WebDriverWait(browser, timeout).until(ec.visibility_of_all_elements_located(
            (By.XPATH, "//input[@id='Username' or @id='Password']")))
    except TimeoutException:
        print("ERROR: Timed out waiting for Noddle site response. Username and/or password field could not be found.")
        try:
            browser.quit()
        except ConnectionAbortedError:
            return -1

    # Log in
    email = browser.find_element_by_id('Username')
    password = browser.find_element_by_id('Password')
    email.send_keys(usr)
    password.send_keys(pwd)
    browser.find_element_by_xpath("//p[@class='relative']/input[@type='submit']").click()

    # Try to wait until pop-up loaded, then click close button, or continue
    try:
        WebDriverWait(browser, popup_timeout).until(ec.visibility_of_element_located((By.CLASS_NAME, 'close-x')))
        browser.find_element_by_class_name('close-x').click()
    except TimeoutException:
        pass

    # Wait until Score loaded, then return it
    try:
        WebDriverWait(browser, timeout).until(ec.visibility_of_element_located((By.CLASS_NAME, 'credit-score')))
        score = int(browser.find_element_by_class_name('credit-score').text)
    except TimeoutException:
        print("ERROR: Timed out waiting for Noddle site response. Credit score element could not be found.")
        try:
            browser.quit()
        except ConnectionAbortedError:
            return -1

    # Quit the browser and return score
    browser.quit()
    return score


# Prevent execution from import statement
if __name__ == "__main__":
    # Load encrypted credentials from creds.json file
    with open('creds.json', 'r') as creds_file:
        creds = json.load(creds_file)

        with open('NaCl', 'rb') as salt_file:
            salt = salt_file.read()

        # Prompt user for master password and create Fernet for creds decryption
        if sys.stdin.isatty():
            f, _ = create_fernet(getpass("Master password: ").encode(), salt)
        else:
            print(
                'Master password (BEWARE key will be visible in console. Please use a TTY terminal to avoid this.):')
            f, _ = create_fernet(sys.stdin.readline().rstrip().encode(), salt)

        # Scrape score and print
        callcredit_max_score = 710
        score = scrape_score(
            f.decrypt(creds['Noddle']['usr'].encode()).decode(),
            f.decrypt(creds['Noddle']['pwd'].encode()).decode())
        print("Callcredit credit score: " + str(score) + " / " + str(callcredit_max_score))
